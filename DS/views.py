from django.shortcuts import render

from DS.models import Visit


def get_views(request):
    visits = Visit.objects.count()
    last_visit = Visit.objects.last()
    Visit(name=str(request.META.get('HTTP_X_FORWARDED_FOR'))).save()
    return render(request, 'index.html', {'visits': visits})
