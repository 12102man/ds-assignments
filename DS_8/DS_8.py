from multiprocessing import Process, Pipe
from datetime import datetime


def process_timestamp(counter):
    return ' (time vector: {}, current time: {})'.format(counter,
                                                         datetime.now())


def send(pipe, pid, counter):
    """
       Method for sending message
       :param pipe: pipe to send
       :param pid: pid of process
       :param counter: vector of processes' times
       :return: updated counter
       """
    counter[pid] += 1
    pipe.send(('test', counter))
    print('Message sent from ' + str(pid) + process_timestamp(counter))
    return counter


def empty_event(pid, counter):
    """
    Empty event for just incrementing counter
    :param pid: pid of process
    :param counter: vector of processes' times
    :return: updated counter
    """
    counter[pid] += 1
    print(f'Empty event | {process_timestamp(counter)}')
    return counter


def receive(pipe, pid, counter):
    """
    Method for receiving message
    :param pipe: pipe to send
    :param pid: pid of process 
    :param counter: vector of processes' times
    :return: updated counter
    """
    message, timestamp = pipe.recv()
    counter = calculate_process_timestamp(pid, timestamp, counter)
    print('Message received at ' + str(pid) + process_timestamp(counter))
    return counter


def calculate_process_timestamp(pid, received, counter):
    """
    Calculates new vector of processes' times through 
    finding max value of each process' time
    :param pid: pid of process
    :param received: vector of processes' times which was received through pipe
    :param counter: current vector
    :return: resulting vector
    """
    out = []
    for i in range(len(received)):
        out.append(max(received[i], counter[i]))
    out[pid] += 1
    return out


def process_a(pipe12):
    pid = 0
    counter = [0, 0, 0]

    counter = send(pipe12, pid, counter)  # a0
    counter = send(pipe12, pid, counter)  # a1
    counter = empty_event(pid, counter)  # a2
    counter = receive(pipe12, pid, counter)  # a3
    counter = empty_event(pid, counter)  # a4
    counter = empty_event(pid, counter)  # a5
    counter = receive(pipe12, pid, counter)  # a6

    print(f'Process {pid} : {counter}')


def process_b(pipe21, pipe23):
    pid = 1
    counter = [0, 0, 0]
    counter = receive(pipe21, pid, counter)  # b0
    counter = receive(pipe21, pid, counter)  # b1
    counter = send(pipe21, pid, counter)  # b2
    counter = receive(pipe23, pid, counter)  # b3
    counter = empty_event(pid, counter)  # b4
    counter = send(pipe21, pid, counter)  # b5
    counter = send(pipe23, pid, counter)  # b6
    counter = send(pipe23, pid, counter)  # b7

    print(f'Process {pid} : {counter}')


def process_c(pipe32):
    pid = 2
    counter = [0, 0, 0]
    counter = send(pipe32, pid, counter)  # c0
    counter = receive(pipe32, pid, counter)  # c1
    counter = empty_event(pid, counter)  # c2
    counter = receive(pipe32, pid, counter)  # c3

    print(f'Process {pid} : {counter}')


if __name__ == '__main__':
    ab, ba = Pipe()
    bc, cb = Pipe()

    process1 = Process(target=process_a,
                       args=(ab,))
    process2 = Process(target=process_b,
                       args=(ba, bc))
    process3 = Process(target=process_c,
                       args=(cb,))

    process1.start()
    process2.start()
    process3.start()

    process1.join()
    process2.join()
    process3.join()
