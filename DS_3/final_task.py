URLS = ["http://www.irs.gov/pub/irs-pdf/f1040.pdf",
"http://www.irs.gov/pub/irs-pdf/f1040a.pdf",
"http://www.irs.gov/pub/irs-pdf/f1040ez.pdf",
"http://www.irs.gov/pub/irs-pdf/f1040es.pdf",
"http://www.irs.gov/pub/irs-pdf/f1040sb.pdf"]

from threading import Thread
import urllib.request as urllib2
import os
import time
from datetime import datetime

class DownloadThread(Thread):   
    def __init__(self, url, name):
        Thread.__init__(self)
        self.name = name
        self.url = url
    
    def run(self):
        handle = urllib2.urlopen(self.url)
        fname = os.path.basename(self.url)
        
        with open(fname, "wb") as f_handler:
            while True:
                chunk = handle.read(1024)
                if not chunk:
                    break
                f_handler.write(chunk)
        
        msg = "%s закончил загрузку %s!" % (self.name, self.url)
        print(msg)


def parallel_threads():
    threads = []
    for item, url in enumerate(URLS):
        name = "Поток %s" % (item+1)
        thread = DownloadThread(url, name)
        threads.append(thread)
        thread.start()
    while True:
        if all([not thread.is_alive() for thread in threads]):
            break


def sequential_threads():
    threads = []
    for item, url in enumerate(URLS):
        name = "Поток %s" % (item+1)
        thread = DownloadThread(url, name)
        threads.append(thread)
        thread.start()
        thread.join()
    while True:
        if all([not thread.is_alive() for thread in threads]):
            break

def parallel_processes():
    processes = []
    for item, url in enumerate(URLS):
        name = "Поток %s" % (item+1)
        process = DownloadThread(url, name)
        processes.append(process)
        process.start()
    while True:
        if all([not process.is_alive() for process in processes]):
            break

def sequential_processes():
    processes = []
    for item, url in enumerate(URLS):
        name = "Поток %s" % (item+1)
        process = DownloadThread(url, name)
        processes.append(process)
        process.start()
        process.join()
    while True:
        if all([not process.is_alive() for process in processes]):
            break

if __name__=='__main__':
    
    start_time = datetime.now()
    parallel_threads()
    print('[PT] Executed in : '+str(datetime.now()-start_time))

    start_time = datetime.now()
    sequential_threads()
    print('[ST] Executed in : '+str(datetime.now()-start_time))

    start_time = datetime.now()
    parallel_processes()
    print('[PP] Executed in : '+str(datetime.now()-start_time))

    start_time = datetime.now()
    sequential_processes()
    print('[SP] Executed in : '+str(datetime.now()-start_time))
    